import React from "react";
import {Route} from "react-router";

import AppController from "./controllers/AppController"
import ArticleController from "./controllers/ArticlesController"
import Home from "./components/Home/Home"

export default (

    <Route component={AppController}>
        <Route path="/" component={Home} />
        <Route path="/articles" component={ArticleController} />
    </Route>

);