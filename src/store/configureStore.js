import thunkMiddleware from 'redux-thunk'
import {createStore, applyMiddleware, compose} from 'redux'
import rootReducer from "../reducers/index"
import promiseMiddleware from "../utils/middleware/PromiseMiddleware"

export default function configureStore(initialState) {

    const devTools = function() {
        return typeof window === 'object' && typeof window.devToolsExtension
        !== 'undefined' ? window.devToolsExtension() : f => f
    };

    const finalCreateStore = compose(
        applyMiddleware(
            thunkMiddleware,
            promiseMiddleware
        ),
        devTools()
    )(createStore);

    let store = finalCreateStore(rootReducer, initialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').default;
            store.replaceReducer(nextRootReducer);
        })
    }

    return store;
};