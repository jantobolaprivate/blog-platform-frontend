export default {
    API_SERVER_HOST: 'localhost',
    API_SERVER_PORT: 8080,
    NODE_SERVER_PORT: 3000
}