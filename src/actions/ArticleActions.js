import {ArticleActionTypes} from "../constants/AppConstants";
import ArticlesApi from "../api/ArticlesApi"

export function fetchArticles() {
    return {
        types: [
            ArticleActionTypes.FETCH_ARTICLES_REQUEST,
            ArticleActionTypes.FETCH_ARTICLES_SUCCESS,
            ArticleActionTypes.FETCH_ARTICLES_FAILURE
        ],
        call: ArticlesApi.getAllArticles()
    }
}

export function fetchTest() {
    return {
        types: [
            ArticleActionTypes.FETCH_ARTICLES_REQUEST + "_TEST",
            ArticleActionTypes.FETCH_ARTICLES_SUCCESS + "_TEST",
            ArticleActionTypes.FETCH_ARTICLES_FAILURE + "_TEST"
        ],
        call: ArticlesApi.getTest()
    }
}