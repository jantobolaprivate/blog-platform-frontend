import keyMirror from "key-mirror";

export const ArticleActionTypes = keyMirror({
    FETCH_ARTICLES_REQUEST: null,
    FETCH_ARTICLES_SUCCESS: null,
    FETCH_ARTICLES_FAILURE: null
});