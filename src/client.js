import { render } from "react-dom";
import React from "react"
import { Provider } from "react-redux";
import { Router, browserHistory } from 'react-router';
import configureStore from "./store/configureStore"
import routes from "./routes";

/**
 * Create store and fetch initial state created during server
 * render.
 */
const store = configureStore(window.__INITIAL_STATE__);

render(

    <Provider store={store}>
        <Router history={browserHistory}>
            {routes}
        </Router>
    </Provider>,

    document.getElementById('root')
);

function check() {
    module.hot.check(function(err, updatedModules) {
        if (updatedModules) {
            console.log(updatedModules);
        }
    });
}

window.onmessage = function(event) {
    console.log(event);
    if (typeof event.data == 'string' && event.data.indexOf("webpackHotUpdate") >= 0) {
        // check();
        debugger
        window.location.reload();
        console.log("lol");
    }
};