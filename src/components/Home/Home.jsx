import React from "react";
import s from './Home.scss'

export default function Home() {
    return (
        <div className={s.headline}>
            <h2>This is HOME!</h2>
        </div>
    );
}