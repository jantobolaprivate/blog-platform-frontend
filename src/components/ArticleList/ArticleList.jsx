import React, {Component} from 'react';
import s from "./ArticleList.scss"

export default class ArticleList extends Component {

    render() {
        var articleTRs = [];

        this.props.articles.data.forEach(article => {
            articleTRs.push(
                <tr key={article.id} className={s.redred}>
                    <td>{article.title}</td>
                    <td>{article.content}</td>
                </tr>
            );
        });

        var result;
        if (this.props.articles.isFetching) {
            result = "Loading...";
        } else {
            result = articleTRs;
        }

        return (
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Content</th>
                    </tr>
                </thead>
                <tbody>
                    {result}
                </tbody>
            </table>
        );
    }

}