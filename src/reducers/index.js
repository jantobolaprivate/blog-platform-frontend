import {combineReducers} from 'redux';
import {articleReducer} from './ArticlesReducer';

const rootReducer = combineReducers({
    articleReducer
});

export default rootReducer;