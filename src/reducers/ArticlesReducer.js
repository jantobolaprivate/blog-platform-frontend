import { ArticleActionTypes } from "../constants/AppConstants";

const initialState = {
    data: [],
    isFetching: false
};

export function articleReducer(state = initialState, action) {

    switch (action.type) {

        case ArticleActionTypes.FETCH_ARTICLES_REQUEST:
            return {...state, ...{
                isFetching: true
            }};
        
        case ArticleActionTypes.FETCH_ARTICLES_SUCCESS:
            return {...state, ...{
                isFetching: false,
                data: action.result.data
            }};

        case ArticleActionTypes.FETCH_ARTICLES_FAILURE:
            return {...state, ...{
                isFetching: false,
                failed: true
            }};
        
        default:
            return state;
    }
}