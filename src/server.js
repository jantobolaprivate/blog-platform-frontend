import express from "express"
import React from "react"
import ReactDOMServer from "react-dom/server"
import {Router, RouterContext, match, createMemoryHistory} from "react-router"
import {Provider} from 'react-redux'
import routes from "./routes"
import configureStore from "./store/configureStore"
import requestProxy from "express-request-proxy"
import {serverPromiseMiddleware} from "./utils/middleware/ServerPromiseMiddleware"
import path from 'path'
import config from "./config"

/**
 * Create server
 */
const app = express();

/**
 * Jade view engine setup
 */
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

/**
 * Serving static files
 */
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Proxy to API server
 */
app.all("/api/*", requestProxy({
    url: `http://${config.API_SERVER_HOST}:${config.API_SERVER_PORT}/*`
}));

/**
 * Request handling & Server rendering
 */
app.get("*", function (req, res, next) {
    
    let history = createMemoryHistory();
    let store = configureStore();

    let router = function () {
        return (
            <Router history={history}>
                {routes}
            </Router>
        );
    };

    match({routes: router(), location: req.url}, (error, redirectLocation, renderProps) => {

        // TODO do it better
        if (redirectLocation) {
            res.redirect(301, redirectLocation.pathname + redirectLocation.search);
        } else if (error) {
            res.status(500).send(error.message);
        } else if (renderProps == null) {
            res.status(404).send('Not found');
        } else {

            // pass it to 'middleware'
            serverPromiseMiddleware(renderProps, store, function () {

                let content = ReactDOMServer.renderToString(
                    <Provider store={store}>
                        { <RouterContext {...renderProps} /> }
                    </Provider>
                );

                let initialState = JSON.stringify(store.getState());
                res.render('index', {content, initialState});
            });
        }
    });
});

/**
 * Start the express server
 *
 * @type {http.Server}
 */
var server = app.listen(config.NODE_SERVER_PORT, function () {
    var port = server.address().port;
    console.log('Node express server listening on port %s', port);
});
