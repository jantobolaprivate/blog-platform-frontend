import RestApi from "./RestApi";

export default {

    getAllArticles: () => {
        return RestApi.get("/api/articles");
    },

    getTest: () => {
        return RestApi.get("/api/test");
    }
    
}