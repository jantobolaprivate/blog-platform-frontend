import axios from "axios";
import config from "../config"

const instance = axios.create({
    baseURL: `http://localhost:${config.NODE_SERVER_PORT}`
});

export default instance;