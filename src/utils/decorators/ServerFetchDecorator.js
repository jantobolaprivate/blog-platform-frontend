/**
 *
 *
 * @param actionArray
 * @returns {function()}
 * @constructor
 */
export function ServerFetch(...actionArray) {
    return (target, name, descriptor) => {

        var arr = [];
        actionArray.forEach((obj) => {
            arr.push(obj());
        });

        target.fetchData = function () {
            return arr
        };
        return descriptor;
    };
}