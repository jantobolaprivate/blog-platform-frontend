/**
 * ServerPromiseMiddleware.js
 */

function type(types) {
    let [REQUEST, SUCCESS, FAILURE] = types;
    return {REQUEST, SUCCESS, FAILURE}
}

function getComponentActions(renderProps, store) {
    let {query, params} = renderProps;
    let component = renderProps.components[renderProps.components.length - 1].WrappedComponent;
    return (component && component.fetchData) ? component.fetchData({query, params, store}) : null;
}

export function serverPromiseMiddleware(renderProps, store, callback) {
    // Promise array
    let calls = [];
    // Actions array or null
    let componentActions = getComponentActions(renderProps, store);
    if (componentActions) {
        // Process all promises
        for (let {call, types, ...rest} of componentActions) {
            calls.push(
                call.then(
                    (result) => {
                        let {SUCCESS} = type(types);
                        store.dispatch({type: SUCCESS, result, ...rest});
                    },
                    (error) => {
                        let {REQUEST, FAILURE} = type(types);
                        console.log(`Error server-side data prefetch (${REQUEST}):`, error);
                        store.dispatch({type: FAILURE, error, ...rest});
                    }
                )
            );
        }
    }
    if (calls.length > 0) {
        // Continue after all promises done
        Promise.all(calls).then(() => {
            callback();
        }).catch((e) => {
            console.log("Fatal server-side data prefetch error: ", e);
            callback();
        });
    } else {
        callback();
    }
}