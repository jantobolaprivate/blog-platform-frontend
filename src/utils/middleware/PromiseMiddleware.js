/**
 * Store middleware that is able to handle promises
 * in actions.
 *
 * @returns {function(): function()}
 */
export default function promiseMiddleware() {

    return (next) => (action) => {

        const {call, types, ...rest} = action;

        if (!call) {
            return next(action);
        }

        const [REQUEST, SUCCESS, FAILURE] = types;
        next({...rest, type: REQUEST});

        return call.then(
            (result) => {
                next({...rest, result, type: SUCCESS})
            },

            (error) => {
                next({...rest, error, type: FAILURE})
            }
        );
    };
}