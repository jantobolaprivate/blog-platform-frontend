import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ServerFetch} from "../utils/decorators/ServerFetchDecorator"
import ArticleList from "../components/ArticleList/ArticleList"
import {fetchArticles, fetchTest} from "../actions/ArticleActions"

/**
 * Redux mapping.
 */
function mapStateToProps(state) {
    return {
        articles: state.articleReducer
    }
}

/**
 * React controller-view
 */
@connect(mapStateToProps, {fetchArticles})
@ServerFetch(fetchArticles)
export default class ArticlesController extends Component {

    reload = () => {
        this.props.fetchArticles();
    };

    render() {
        return (
            <div>
                <h2>All articles in table below:</h2>
                <ArticleList articles={this.props.articles}/>
                <button onClick={this.reload}>Reload</button>
            </div>
        );
    }

}