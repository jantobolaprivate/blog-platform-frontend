import React, {Component} from "react";
import Footer from "../components/Footer/Footer"

/**
 * Main layout
 */
export default class AppController extends Component {
    render() {
        return (
            <div>

                <h1>Blog frontend!</h1>

                <hr/>
                {this.props.children}
                <hr/>

                <Footer/>

            </div>
        );
    }
}