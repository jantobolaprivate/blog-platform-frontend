import webpack from "webpack"
import WebpackDevServer from "webpack-dev-server"
import webpackDevConfig from "./webpack.config.dev"
import runScript from "./runScript"
import Promise from 'bluebird'
import fs from 'fs'

const DEBUG = !process.argv.includes('--release');

async function start() {
    if (DEBUG) startDev();
    else startRelease();
}

async function startDev() {

    const dir = "./build";
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    // Copy static files
    const ncp = Promise.promisify(require('ncp'));
    await Promise.all([
        ncp('src/public', 'build/public'),
        ncp('src/views', 'build/views')
    ]);

    var bundle = new Promise(function (resolve, reject) {
        // bundle app using webpack
        console.log("Building app using webpack for DEVELOPMENT environment...");
        webpack(webpackDevConfig, function (err, stats) {
            if (err) {
                reject();
                throw new Error("Webpack bundle error.", err);
            }
            console.log("Bundle done: ", stats.toString({}));
            resolve();
        });
    });

    bundle.then(() => {
        // Start an express server
        runScript("build/server.js", (err) => {
            console.log("Error during running server script: ", err);
        });

        // Start a webpack-dev-server
        new WebpackDevServer(webpack(webpackDevConfig[0]), {
            publicPath: webpackDevConfig[0].output.publicPath,
            hot: true,
            stats: {colors: true}
        }).listen(3030, "localhost", function (err) {
            if (err) throw new Error("webpack-dev-server", err);
            console.log("Webpack-dev-server started, address: http://localhost:3030");
        });
    });
}

async function startRelease() {
    console.log("Release not configured yet.");
}

export default start;