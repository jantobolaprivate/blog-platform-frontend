import path from 'path'
import webpack from 'webpack'
import extend from 'extend'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import precss from 'precss'
import autoprefixer from 'autoprefixer'
import postcssImport from 'postcss-import'

/**
 * Common config (client.js & server.js)
 */
const config = {

    output: {
        path: path.resolve(__dirname, '../build'),
        publicPath: 'http://localhost:3030/build'
    },

    resolve: {
        // you can now require('file') instead of require('file.components')
        extensions: ['', '.js', '.jsx']
    },

    plugins: [
        new ExtractTextPlugin("public/styles.css", {
            allChunks: true
        })
    ],

    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loaders: ['react-hot', 'babel'],
                exclude: /node_modules/
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1!sass')
            }
        ]
    }

};

/**
 * client.js specific config
 */
const clientConfig = extend(true, {}, config, {

    devtool: 'eval',

    output: {
        filename: 'public/bundle.js'
    },

    entry: [
        'webpack-dev-server/client?http://localhost:3030',
        'webpack/hot/dev-server',
        './src/client'
    ],

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ].concat(config.plugins),

    target: 'web'
});

/**
 * server.js specific config
 */
const serverConfig = extend(true, {}, config, {

    output: {
        filename: 'server.js',
        libraryTarget: 'commonjs2'
    },

    externals: [
        /^\.\/build$/,
        // wtf?
        function filter(context, request, cb) {
            const isExternal =
                request.match(/^[@a-z][a-z\/\.\-0-9]*$/i) && !request.match(/^react-routing/) && !context.match(/[\\/]react-routing/);
            cb(null, Boolean(isExternal));
        }
    ],

    plugins: [
        new webpack.DefinePlugin({'process.env.BROWSER': false}),
    ].concat(config.plugins),

    entry: './src/server',
    target: 'node',

    node: {
        console: false,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false
    }
});

export default [clientConfig, serverConfig];